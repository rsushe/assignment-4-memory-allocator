//
// Created by rsushe on 12/3/22.
//
#define _DEFAULT_SOURCE

#include "tests.h"

void free_heap(void *heap, size_t length) {
    munmap(heap, size_from_capacity((block_capacity) {.bytes = length}).bytes);
}

bool success_memory_allocate() {
    printf("TEST1 STARTED\n");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    void *data = _malloc(1000);
    debug_heap(stdout, heap);

    if (!data) {
        printf("TEST1 FAILED\n");
        return false;
    }

    _free(data);

    debug_heap(stdout, heap);

    free_heap(heap, REGION_MIN_SIZE);

    printf("TEST1 SUCCEED\n");
    return true;
}

bool success_free_one_block_out_of_several_allocated() {
    printf("TEST2 STARTED\n");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    void *data1 = _malloc(1000);
    void *data2 = _malloc(1000);
    void *data3 = _malloc(1000);

    debug_heap(stdout, heap);

    if (!data1 || !data2 || !data3) {
        printf("TEST2 FAILED\n");
        return false;
    }

    _free(data2);

    debug_heap(stdout, heap);

    if (!data1 || !data3) {
        printf("TEST2 FAILED\n");
        return false;
    }
    _free(data1);
    _free(data3);
    free_heap(heap, REGION_MIN_SIZE);

    printf("TEST2 SUCCEED\n");
    return true;
}

bool success_free_two_block_out_of_several_allocated() {
    printf("TEST3 STARTED\n");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    void *data1 = _malloc(1000);
    void *data2 = _malloc(1000);
    void *data3 = _malloc(1000);

    debug_heap(stdout, heap);

    if (!data1 || !data2 || !data3) {
        printf("TEST3 FAILED\n");
        return false;
    }

    _free(data2);
    _free(data1);

    debug_heap(stdout, heap);

    if (!data3) {
        printf("TEST3 FAILED\n");
        return false;
    }

    _free(data3);
    free_heap(heap, REGION_MIN_SIZE);

    printf("TEST3 SUCCEED\n");
    return true;
}

bool grow_heap_test_right_after_heap() {
    printf("TEST4 STARTED\n");
    bool flag = false;

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    void *huge_data = _malloc(REGION_MIN_SIZE * 2);
    debug_heap(stdout, heap);

    struct block_header const *header = heap;
    if (header->capacity.bytes > REGION_MIN_SIZE) {
        flag = true;
    }

    _free(huge_data);
    free_heap(heap, REGION_MIN_SIZE);

    if (flag) printf("TEST4 SUCCEED\n");
    else printf("TEST4 FAILED\n");

    return flag;
}

bool grow_heap_test_if_right_after_is_placed() {
    printf("TEST5 STARTED\n");

    void *heap = heap_init(1000);
    debug_heap(stdout, heap);


    void *data1 = _malloc(900);
    debug_heap(stdout, heap);

    if (!data1) {
        printf("TEST5 FAILED\n");
        return false;
    }

    struct block_header const *header = (struct block_header*) (data1 - offsetof(struct block_header, contents));

    (void) mmap((void *) header->contents + header->capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE,
                  MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);

    void *data2 = _malloc(1000);
    debug_heap(stdout, heap);

    if (!data2) {
        printf("TEST5 FAILED\n");
        return false;
    }

    _free(data1);
    _free(data2);
    free_heap(heap, REGION_MIN_SIZE);

    printf("TEST5 SUCCEED\n");

    return true;
}


testFuncDef tests[] = {
        success_memory_allocate,
        success_free_one_block_out_of_several_allocated,
        success_free_two_block_out_of_several_allocated,
        grow_heap_test_right_after_heap,
        grow_heap_test_if_right_after_is_placed
};

void run_tests() {
    int successfully_passed_tests = 0;
    for (int i = 0; i < TESTS_COUNT; i++) {
        if (tests[i]()) {
            successfully_passed_tests++;
        }
    }

    printf("Passed %d tests out of %d tests\n", successfully_passed_tests, TESTS_COUNT);
    printf("Failed %d tests out of %d tests", TESTS_COUNT - successfully_passed_tests, TESTS_COUNT);
}