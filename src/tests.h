//
// Created by rsushe on 12/3/22.
//

#ifndef MEMORY_ALLOCATOR_TESTS_H
#define MEMORY_ALLOCATOR_TESTS_H

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <sys/mman.h>

#define TESTS_COUNT 5

typedef bool (*testFuncDef)();

void run_tests();

#endif //MEMORY_ALLOCATOR_TESTS_H
